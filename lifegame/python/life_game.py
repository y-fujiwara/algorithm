from collections import namedtuple
import matplotlib.pyplot as plt
import matplotlib.animation as anm
import matplotlib.patches as patches
import numpy as np

ALIVE = "*"
EMPTY = "-"
TICK = object()

# namedtupleでJavaBeans的なクラスを自動で生成できる
Query = namedtuple("Query", ("y", "x"))
Transition = namedtuple("Transition", ("y", "x", "state"))

def count_neighbors(y, x):
    """対象点の近隣情報を設定、取得する関数
    
    Arguments:
        y {int} -- y座標
        x {int} -- x座標
    
    Yields:
        Query -- 座標インスタンス
    """

    # n_とかにはsendの引数で与えられたものが入る
    n_ = yield Query(y + 1, x + 0) # 北
    ne = yield Query(y + 1, x + 1) # 北東
    e_ = yield Query(y + 0, x + 1) # 東
    se = yield Query(y - 1, x + 1) # 南東
    s_ = yield Query(y - 1, x + 0) # 南
    sw = yield Query(y - 1, x - 1) # 南西
    w_ = yield Query(y + 0, x - 1) # 西
    nw = yield Query(y + 1, x - 1) # 北西

    neighbor_states = [n_, ne, e_, se, s_, sw, w_, nw]
    count = 0
    for state in neighbor_states:
        if state is ALIVE:
            count += 1
    return count

def step_cell(y, x):
    """マス目の座標を受取座標の状態を計算後
    count_neighborsを実行して周りのセルを調べる
    ゲームの規則を適用し次のステップ状態を決定する
    
    Arguments:
        y {int} -- y座標
        x {int} -- x座標
    
    Yields:
        str -- 次ステップの状態
    """

    state = yield Query(y, x)
    # yield fromによる合成
    # from以下に与えられたジェネレータが使い切られるとneighborsに結果が入る
    # ループなどで呼ばれるたびにcount_neighborsのyieldが実行される
    neighbors = yield from count_neighbors(y, x)
    next_state = game_logic(state, neighbors)
    yield Transition(y, x, next_state)

def game_logic(state, neighbors):
    """ゲームロジック本体
    隣接セルが2未満かつ3より大きく生の場合は対象セルは死
    隣接セルが3つ生きている場合は生
    
    Arguments:
        state {str} -- 対象セルの死活状態
        neighbors {int} -- 隣接セルの状態結果
    
    Returns:
        str -- 対象セルの新規状態
    """

    if state is ALIVE:
        if neighbors < 2:
            return EMPTY # 死 あまりに少ない
        elif neighbors > 3:
            return EMPTY # 死 あまりに多い
    else:
        if neighbors is 3:
            return ALIVE # 再生
    return state

def simulate(height, width):
    """一回分のステップ実行
    
    Arguments:
        height {int} -- 盤面の高さ
        width {int} -- 盤面の幅
    
    Yields:
        Transition | object -- 各点の状態インスタンス
    """

    while True:
        for y in range(height):
            for x in range(width):
                yield from step_cell(y, x)
        yield TICK

def live_a_generation(grid, sim):
    """盤面とシュミレーターを使って全盤面の死活状態をアップデートする
    
    Arguments:
        grid {Grid} -- 元の盤面
        sim {Transition | object} -- シミュレーション状態
    
    Returns:
        Grid -- アップデート後の盤面状況
    """

    progeny = Grid(grid.height, grid.width)
    item = next(sim)
    while item is not TICK:
        if isinstance(item, Query):
            state = grid.query(item.y, item.x)
            item = sim.send(state)
        else:
            progeny.assign(item.y, item.x, item.state)
            item = next(sim)
    return progeny

class Grid():
    """ライフゲーム盤面全体を表すクラス
    """

    def __init__(self, height, width):
        """コンストラクタ
        
        Arguments:
            height {int} -- 盤面の高さ
            width {int} -- 盤面の幅
        """

        self.height = height
        self.width = width
        self.rows = []
        for _ in range(self.height):
            self.rows.append([EMPTY] * self.width)

    def __str__(self):
        """文字列化メソッド
        
        Returns:
            str -- 盤面の状況を文字列として表現したstrインスタンス
        """

        ret = ''
        for i in range(self.height):
            for j in range(self.width):
                ret += self.rows[i][j]
            ret += "\n"
        return ret

    def plot(self):
        """Matplotlibを使った盤面描画メソッド
        """

        plt.cla() 

        # 座標のラベルを第1象限で1刻みに変更
        plt.xticks(np.arange(0, self.width, 1))
        plt.yticks(np.arange(0, self.height, 1))

        # 座標の表示領域をインスタンス変数の幅と高さ分に
        plt.xlim(0, self.width) 
        plt.ylim(0, self.height)

        # 格子を表示する
        plt.grid()

        for i in range(self.height):
            for j in range(self.width):
                if self.rows[i][j] is ALIVE:
                    rect = plt.Rectangle(xy=(j, i), width=1, height=1, fill="#770000") 
                    # gca stands for 'get current axis'
                    plt.gca().add_patch(rect)

    def query(self, y, x):
        """指定された座標点の状態を取得するメソッド
        
        Arguments:
            y {int} -- y座標
            x {int} -- x座標
        
        Returns:
            str -- '*' or '-'
        """

        return self.rows[y % self.height][x % self.width]

    def assign(self, y, x, state):
        """指定された座標の状態を更新するメソッド
        
        Arguments:
            y {int} -- y座標
            x {int} -- x座標
            state {str} -- '*' or '-'
        """

        self.rows[y % self.height][x % self.width] = state

def run(height, width):
    """連続描画発火関数取得関数
    
    Arguments:
        height {int} -- 盤面の高さ
        width {int} -- 盤面の幅
    
    Returns:
        function -- クロージャによる盤面描画開始関数
    """

    grid = Grid(width, height)
    sim = simulate(grid.height, grid.width)
    grid.assign(0, 3, ALIVE)
    grid.assign(1, 4, ALIVE)
    grid.assign(2, 2, ALIVE)
    grid.assign(2, 3, ALIVE)
    grid.assign(2, 4, ALIVE) 

    grid.assign(10, 10, ALIVE)
    grid.assign(12, 11, ALIVE)
    grid.assign(10, 12, ALIVE)
    grid.assign(9, 11, ALIVE)

    grid.assign(18, 18, ALIVE)
    grid.assign(19, 18, ALIVE)
    grid.assign(20, 18, ALIVE)

    def do_run(i):
        nonlocal grid, sim
        grid.plot()
        grid = live_a_generation(grid, sim)
    return do_run

if __name__ == "__main__":
    width = 20
    height = 20

    fig = plt.figure() 
    ani = anm.FuncAnimation(fig, run(height, width), frames = 360, interval = 5) 
    # ani.save('lifegame.gif', writer='imagemagick', fps=10);
    plt.show()


