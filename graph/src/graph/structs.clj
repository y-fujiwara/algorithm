(ns graph.structs)

(defn pair [v w] {:v v :w w})

(defn init-graph-vec [n]
  "n個の要素の隣接ベクトルを生成する ノードの番号をインデックス番号とする"
  (list (repeat n nil)))

(defn init-graph-matrix [n]
  "n個の要素の隣接行列を生成する ノードの番号をインデックス番号とする"
  (list (repeat n (list (repeat n nil)))))

(defn add-vec [vec s r w]
  "vec:  更新対象隣接ベクタ, s: 始点, r: 接続点, w: 重み"
  (let [target (nth vec s)
        p (pair r w)
        l (cons p target)]
    (assoc vec s l)))
