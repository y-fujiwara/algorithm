class Car():
    def __init__(self, number, gas, total_mileage):
        self.__number = number
        self.__gas = gas 
        self.__total_mileage = total_mileage

    @property
    def number(self):
        return self.__number

    @property
    def gas(self):
        return self.__gas

    @property
    def total_mileage(self):
        return self.__total_mileage