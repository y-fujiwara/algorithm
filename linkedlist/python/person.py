class Person():
    def __init__(self, name, age, weight, height):
        self.__name = name
        self.__age = age
        self.__weight = weight
        self.__height = height

    @property
    def name(self):
        return self.__name

    @property
    def age(self):
        return self.__age

    @property
    def weight(self):
        return self.__weight

    @property
    def height(self):
        return self.__height