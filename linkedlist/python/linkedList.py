from node import Node
from typing import TypeVar, Generic

T = TypeVar('T')

class LinkedList(Generic[T]):
    def __init__(self):
        self.__head = None
        self.__crnt = None

    def __str__(self):
        ret = "["
        temp = self.__head
        if temp is None:
            return ret + "]"
        while temp.next is not None:
            ret = ret + str(temp.data) + ","
            temp = temp.next
        ret = ret + str(temp.data) + "]"
        return ret

    def unshift(self, data: T):
        n = Node[T](data)
        n.next = self.__head
        self.__head = self.__crnt = n

    def shift(self):
        ret = self.__head

        if ret is None:
            return None

        self.__head = self.__head.next
        self.__crnt = self.__head
        return ret.data

    def push(self, data: T):
        if self.__head is None:
            self.unshift(data)
        else:
            n = Node[T](data)
            pre = self.__head
            while pre.next is not None:
                pre = pre.next
            pre.next = n
            self.__crnt = n
    
    def pop(self):
        pre = self.__head
        if pre is None:
            return None

        # 次がいきなりNoneの場合は一つしかデータがない場合
        if pre.next is None:
            self.__head = None
            self.__crnt = None
            return pre.data

        while pre.next.next is not None:
            pre = pre.next
        self.__crnt = pre
        ret = pre.next.data
        pre.next = None
        return ret
 
    @property
    def crnt(self):
        return self.__crnt
  
    @property
    def head(self):
        return self.__head

if __name__ == '__main__':
    from car import Car
    from person import Person
    l = LinkedList[int]()
    l.push(199)
    l.push(200)
    l.push(300)
    l.push(3300)
    print(l)

    l_car = LinkedList[Car]()
    l_car.push(Car(100, 200, 1000))
    l_car.push(Car(12, 5, 100000))
    l_car.push(Car(31, 48, 2910))
    l_car.push(Car(87, 81, 165))
    l_car.push(Car(770, 80, 987))
    print(l_car)

    l_person = LinkedList[Person]()
    l_person.push(Person("fujiwara", 27, 67, 170))
    l_person.push(Person("yoshiaki", 57, 55, 169))
    l_person.push(Person("tomnaga", 25, 60, 173))
    l_person.push(Person("test", 57, 100, 270))
    l_person.push(Person("aaa", 297, 907, 1070))
    print(l_person)
