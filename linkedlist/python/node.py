from typing import TypeVar, Generic

T = TypeVar('T')

class Node(Generic[T]): 
    def __init__(self, data: T):
        self.__next = None
        self.__data = data

    def __str__(self):
        return str(self.__data)
    
    @property
    def next(self):
        return self.__next
 
    @next.setter
    def next(self, next):
        self.__next = next
 
    @property
    def data(self) -> T:
        return self.__data
