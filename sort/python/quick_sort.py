# !usr/bin/env python
# coding:utf-8

def quick_sort(lst): 
    if len(lst) is 0:
        return lst
    # 配列の真ん中がピボット
    pivot = lst[0]
    return quick_sort([x for x in lst if x < pivot]) + [x for x in lst if x is pivot] + quick_sort([x for x in lst if x > pivot])

def tail_quick(lst, cont):
    if len(lst) is 0:
        return cont([])
    pivot = lst[0]
    left = [x for x in lst if x < pivot]
    center = [x for x in lst if x is pivot]
    right = [x for x in lst if x > pivot]
    return tail_quick(left, lambda low: tail_quick(right, lambda up: cont(low + center + up)))

if __name__ == '__main__':
    import random
    import time
    start = time.time()
    target = [random.randint(1, 1000) for i in range(1000)]
    # elapsed_time:84.17921209335327[sec]
    # print(quick_sort(target))# , lambda x: x))
    print(tail_quick(target, lambda x: x))
    elapsed_time = time.time() - start
    print ("elapsed_time:{0}".format(elapsed_time) + "[sec]")
