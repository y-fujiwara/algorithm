def heap_sort(array):
    print(array)
    # 全体のヒープを構築
    build_heap(array)
    print(array)
    # ヒープの先頭が最大になっているのでそれを順次取り出す
    for i in range(len(array) - 1, 0, -1):
        # ヒープ構築完了としてルートの根である最大値を配列の最後尾に寄せる
        array[0], array[i] = array[i], array[0]
        # ヒープから先頭を取り出した場合は再構築が必要となる
        array = heapify(array, 0, i)
    return array

def build_heap(array):
    length = len(array)
    # 木の根は自分のidx * 2から1と2を足した部分にあるので二分の一から始めれば良い
    for i in range(int(length/2) - 1, -1, -1):
        array = heapify(array, i, length)
    return array

def heapify(array, idx, length):
    while 1:
        # 初期根が子より小さくないと仮定
        largest = idx
        left = idx * 2 + 1
        right = idx * 2 + 2

        # 親子の大きさの確認
        if left < length and array[left] > array[idx]:
            largest = left
        if right < length and array[right] > array[largest]:
            largest = right

        if largest is not idx:
            array[idx], array[largest] = array[largest], array[idx]
            # 値の交換が発生した場合はそこから子を確認するためidxを更新
            idx = largest
        else:
            # 値の交換が行われない場合はループ終わり
            break
    return array

if __name__ == "__main__":
    import random
    target = [random.randint(1, 100) for i in range(20)]
    print(heap_sort(target))
