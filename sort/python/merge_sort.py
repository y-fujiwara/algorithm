#!usr/bin/env python
# coding:utf-8
import sys

def merge(lst):
    length = len(lst)
    
    if length is 1:
        return lst
    elif length is 2:
        if lst[0] > lst[1]:
            lst[0], lst[1] = lst[1], lst[0]
        return lst
    else:
        return merge_exec(merge(lst[:int(length/2)]), merge(lst[int(length/2):]))
    
def merge_exec(left, right):
    ret = []
    # leftおよびrightが空になるまで繰り返す
    while left or right:
        l = left[0] if left else sys.maxsize
        r = right[0] if right else sys.maxsize
        if l < r:
            ret.append(l)
            left.remove(l)
        else:
            ret.append(r)
            right.remove(r)
    return ret
        
if __name__ == '__main__':
    import random
    target = [random.randint(1, 100) for i in range(20)]
    print(merge(target))