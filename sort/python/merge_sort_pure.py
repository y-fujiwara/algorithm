#!usr/bin/env python
# coding:utf-8
import sys

def __swap(lst):
    if (len(lst) is 2) and (lst[0] > lst[1]):
        lst[0], lst[1] = lst[1], lst[0]
    return lst

def __make_sublist(lst):
    ret = [lst[:]]
    # 要素の中に一つでも大きさが2より大きいリストがある間繰り返し
    while any(len(x) > 2 for x in ret):
        # retそのものを変更していくため副作用が起きるのでlengthを最初に定義
        length = len(ret)
        for i in range(0, length):
            elength = len(ret[i])
            # 中身の入れ子リストの大きさが2以下の場合は分割済みなのでそのままappend
            if elength <= 2:
                ret.append(ret[i])
                continue
            # 分割して結果リストにappend
            ret.append(ret[i][:int(elength / 2)])
            ret.append(ret[i][int(elength / 2):])
        # ループの一つ前の要素を削除
        del ret[0: length] 
    # 分割済みリストをそれぞれ整列
    ret = map(__swap, ret)
    return list(ret)

def __merge_exec(left, right):
    ret = []
    while left or right:
        l = left[0] if left else sys.maxsize
        r = right[0] if right else sys.maxsize
        if l < r:
            ret.append(l)
            left.remove(l)
        else:
            ret.append(r)
            right.remove(r)
    return ret

def merge_sort_pure(lst):
    sublist = __make_sublist(lst)
    print(sublist)
    ret = sublist[:]
    while len(ret[0]) is not len(lst):
        length = len(ret)
        # 隣り合う者同士のループなので一つ飛ばしでインデックスを増やす
        for i in range(0, length, 2):
            # 配列の長さが奇数になる場合の特別処理
            if i + 1 is length:
                ret.append(ret[i])
                break
            ret.append(__merge_exec(ret[i], ret[i + 1]))
        # 一つ前のループの対象を削除
        del ret[0: length]
    return ret[0]

if __name__ == '__main__':
    import random
    target = [random.randint(1, 100) for i in range(20)]
    print(merge_sort_pure(target))