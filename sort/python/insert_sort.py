#!usr/bin/env python
# coding:utf-8

def insert_sort(lst):
    for i in range(1, len(lst)):
        # すでにソート済みの部分との比較用ループ
        # rangeの第二引数の終了部分は等号無しなことに注意
        for j in range(i, 0, -1):
            if lst[j] < lst[j - 1]:
                # 比較対象の挿入位置まで各要素をずらしていくイメージ
                lst[j], lst[j - 1] = lst[j - 1], lst[j]
            else:
                # 比較時にlst[j]が比較対象より大きければそれ以降のものよりlst[j]が小さくなることはない
                break
    return lst

if __name__ == '__main__':
    import random
    target = [random.randint(1, 100) for i in range(20)]
    print(insert_sort(target))