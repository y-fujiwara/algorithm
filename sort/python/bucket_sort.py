#!usr/bin/env python
# coding:utf-8
import functools

def bucket_sort(lst, max):
    bucket = [0 for i in range(max + 1)]
    ret = []
    for i in lst:
        bucket[i] += 1

    for (i, elem) in enumerate(bucket):
        ret += [i for j in range(elem)]
    return ret

if __name__ == '__main__':
    import random
    max = 100
    target = [random.randint(1, max) for i in range(20)]
    print(target)
    print(bucket_sort(target, max))