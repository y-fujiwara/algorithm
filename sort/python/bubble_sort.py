#!usr/bin/env python
# coding:utf-8

def bubble_sort(lst):
    for i in range(0, len(lst)):
        for j in range(1, len(lst) - i):
            if lst[j] < lst[j - 1]:
                lst[j - 1], lst[j] = lst[j], lst[j - 1]
    return lst

if __name__ == '__main__':
    import random
    target = [random.randint(1, 100) for i in range(20)]
    print(bubble_sort(target))