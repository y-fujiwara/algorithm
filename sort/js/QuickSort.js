let quickSort = function (lst) {
  if (lst.length === 0) {
    return lst;
  }
  let left = lst.filter(function (elem) {
    return elem < lst[0];
  });
  let right = lst.filter(function (elem) {
    return elem > lst[0];
  });
  let center = lst.filter(function (elem) {
    return elem === lst[0];
  });
  return quickSort(left).concat(center).concat(quickSort(right));
}

let target = [];
let max = 10;
for (let i = 0; i < max; i++) {
  target.push(Math.floor(Math.random() * max));
}
console.log("入力:");
console.log(target);
console.log("出力:");

console.log(quickSort(target));