let bubbleSort = function (list) {
  for (let i = 0; i < list.length; i++) {
    for (let j = 1; j < list.length - i; j++) {
      if (list[j] < list[j - 1]) {
        let swp = list[j];
        list[j] = list[j - 1];
        list[j - 1] = swp;
      }
    }
  }
  return list;
}

let target = [];
let max = 100;
for (let i = 0; i < max; i++) {
  target.push(Math.floor(Math.random() * max));
}
console.log("入力:");
console.log(target);
console.log("出力:");

console.log(bubbleSort(target));