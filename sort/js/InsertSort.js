let insertSort = function (lst) {
  for (let i = 1; i < lst.length; i++) {
    for (let j = i; j > 0; j--) {
      if (lst[j] < lst[j - 1]) {
        let swp = lst[j];
        lst[j] = lst[j - 1];
        lst[j - 1] = swp;
      } else {
        break;
      }
    }
  }
  return lst;
}

let target = [];
let max = 100;
for (let i = 0; i < max; i++) {
  target.push(Math.floor(Math.random() * max));
}
console.log("入力:");
console.log(target);
console.log("出力:");

console.log(insertSort(target));