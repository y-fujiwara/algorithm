let bucketSort = function (lst, max) {
  let bucket = new Array(max);
  lst.forEach(function (elem) {
    bucket[elem] = bucket[elem] !== undefined ? bucket[elem] + 1 : 1;
  });

  let ret = [];
  bucket.forEach(function (elem, index) {
    for (let i = 0; i < elem; i++) {
      ret.push(index);
    }
  });
  return ret;
};

let target = [];
let max = 10;
for (let i = 0; i < max; i++) {
  target.push(Math.floor(Math.random() * max));
}
console.log("入力:");
console.log(target);
console.log("出力:");
console.log(bucketSort(target, max));