let merge = function (left, right) {
  ret = []
  // leftおよびrightが空になるまで繰り返す
  while (left.length !== 0 || right.length !== 0) {
    let l = Number.MAX_VALUE;
    let r = Number.MAX_VALUE;
    if (left.length !== 0) {
      l = left[0];
    }
    if (right.length !== 0) {
      r = right[0];
    }
    if (l < r) {
      ret.push(l);
      left.shift();
    } else {
      ret.push(r);
      right.shift();
    }
  }
  return ret
};


let mergeSort = function (lst) {
  let len = lst.length;
  if (len === 1) {
    return lst;
  } else if (len === 2) {
    if (lst[0] > lst[1]) {
      let swp = lst[0];
      lst[0] = lst[1];
      lst[1] = swp;
    }
    return lst
  } else {
    return merge(mergeSort(lst.slice(0, len / 2)), mergeSort(lst.slice(len / 2, len)));
  }
};

let target = [];
let max = 10;
for (let i = 0; i < max; i++) {
  target.push(Math.floor(Math.random() * max));
}
console.log("入力:");
console.log(target);
console.log("出力:");

console.log(mergeSort(target));