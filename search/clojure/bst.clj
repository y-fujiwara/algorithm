; binary search treeの略
(defn- create-node [value] {:value value :left nil :right nil :height 0})

(defn- get-height [left right]
  (cond
    (and left right) (+ (max (:height left) (:height right) 1))
    left (+ (:height left) 1)
    right (+ (:height right) 1)
    :else 0))

(defn- compute-height {value :value left :left right :right height :height :as node}
  (assoc node :height (get-height left right)))

(defn- add-node
  [{value :value left :left right :right height :height :as node} data]
  (into {} (lazy-seq
            (if (<= data value)
              (if (nil? left)
                (assoc node :left (create-node data))
                (assoc node :left (add-node (node :left) data)))
              (if (nil? right)
                (assoc node :right (create-node data))
                (assoc node :right (add-node (node :right) data)))))))

(defn- add-tree [root value]
  (if (nil? root)
    (create-node value)
    (add-node root value)))

(defn- contains [tree target]
  (loop [node tree]
    (cond
      (nil? node) false
      (< target (:value node)) (recur (:left node))
      (> target (:value node)) (recur (:right node))
      (= target (:value node)) true
      :else false)))

(defn- create-tree [array]
  (loop [array array tree nil]
    (if (= (count array) 0)
      tree
      (recur (rest array) (add-tree tree (first array))))))

(defn binary-search-tree [array target]
  (-> array
      create-tree
      (contains target)))

(println (binary-search-tree [6, 3, 7, 4, 8, 19] 0))
(println (binary-search-tree [6, 3, 7, 4, 8, 19] 19))
(println (binary-search-tree [6, 3, 7, 4, 8, 19] 6))

