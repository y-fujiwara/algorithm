(def ^:private table-size 6)

(defn- generate-hash [val]
  (if (not val)
    nil
    (let [code (.hashCode val)]
      (cond
        (< code 0) (mod (* -1 code) table-size)
        :else (mod code table-size)))))

(defn- hash-load [array]
  (loop [hash-table (vec (repeat table-size [])) array array]
    (let [head (first array) rest (rest array)
          hash-val (generate-hash head) vals (nth hash-table hash-val)]
      (cond
        (zero? (count array)) hash-table
        :else (recur (assoc hash-table hash-val (conj vals head)) rest)))))

(defn- inner-check [inner-array target]
  (loop [head (first inner-array) array inner-array]
    (cond
      (zero? (count array)) false
      (.equals head target) true
      :else (recur (first array) (rest array)))))

(defn- search-exec [table target]
  (let [table-val (nth table (generate-hash target))]
    (cond
      (zero? (count table-val)) false
      :else (inner-check table-val target))))

(defn hash-search [array target]
  (-> array
      hash-load
      (search-exec target)))

(println (hash-search ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "bbb"))