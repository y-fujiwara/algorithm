(defn linear-search [array target]
  (loop [head (first array) tail (rest array)]
    (cond
      (= head target) true
      (= (count tail) 0) false
      :else (recur (first tail) (rest tail)))))
(println (linear-search [6, 3, 7, 4, 8, 19] 0))
(println (linear-search [6, 3, 7, 4, 8, 19] 19))
(println (linear-search [6, 3, 7, 4, 8, 19] 6))