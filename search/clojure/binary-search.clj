(defn binary-search [array target]
  (loop [harf (sort array)]
        ;; シーケンスの要素数の半分を取得 (quotは割り算)
    (let [mid (quot (count harf) 2)]
            ;; シーケンスの中間要素がターゲット値より大きいかどうか調べる
      (cond
        (= (count harf) 0) false
        (= (nth harf mid) target) true
        (< (nth harf mid) target) (recur (drop (+ mid 1) harf))
        (> (nth harf mid) target) (recur (take mid harf))
        :else false))))

(println (binary-search [6, 3, 7, 4, 8, 19] 0))
(println (binary-search [6, 3, 7, 4, 8, 19] 20))
(println (binary-search [6, 3, 7, 4, 8, 19] 19))
(println (binary-search [6, 3, 7, 4, 8, 19] 6))
(println (binary-search [6, 3, 7, 4, 8, 19] 3))
(println (binary-search [6, 3, 7, 4, 8, 19] 7))
(println (binary-search [6, 3, 7, 4, 8, 19] 4))
(println (binary-search [6, 3, 7, 4, 8, 19] 8))