(defn- add-bits [bit data size fns]
  (loop [bit bit func (first fns) funcs (rest fns)]
    (cond
      (nil? func) bit
      :else (recur (bit-or bit (bit-shift-left 1 (func data size))) (first funcs) (rest funcs)))))

(defn- contains [bit value size fns]
  (loop [func (first fns) funcs (rest fns)]
    (cond
      (nil? func) true
      (= (bit-and bit (bit-shift-left 1 (func value size))) 0) false
      :else (recur (first funcs) (rest funcs)))))

(defn- array-add-bits [array size fns]
  (loop [bit 0 head (first array) tail (rest array)]
    (cond
      (nil? head) bit
      :else (recur (add-bits bit head size fns) (first tail) (rest tail)))))

(defn bloom-filter
  ([array value]
   (let [size 1000 fns [(fn [e s] (mod (.hashCode e) s))]]
     (contains (array-add-bits array size fns) value size fns)))
  ([array value size]
   (let [fns [(fn [e size] (mod (.hashCode e) size))]]
     (contains (array-add-bits array size fns) value size fns)))
  ([array value size fns] (contains (array-add-bits array size fns) value size fns)))


(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "agreressd"))
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "aaaaaaa"))
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "bbbb"))
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "cccc"))
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "dddd"))
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "aaaaaa"))
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "fsawe"))
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "test111"))
; "test"は偽陽性に引っかかる
(println (bloom-filter ["aaaaaaa", "bbbb", "cccc", "dddd", "aaaaaa", "fsawe", "agreressd"] "test"))